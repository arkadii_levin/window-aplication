#include <SFML/Graphics.hpp>
#include <iostream>
#include <string>

using namespace std;
using namespace sf;

int main() {

	RenderWindow wnd( VideoMode(1080, 1080), "Window");

	Texture texture1,
			texture2,
			texture3,
			texture4;

	texture1.loadFromFile("download.jpg");
	texture2.loadFromFile("121-500x500.jpg");
	texture3.loadFromFile("cat-2143332_1280.jpg");
	texture4.loadFromFile("cat.jpg");

	Sprite sprite1(texture1),
		   sprite2(texture2),
		   sprite3(texture3),
		   sprite4(texture4);

	bool indicator = true;

	while (wnd.isOpen()) {
		Event event;
		while (wnd.pollEvent(event)) {
			if (event.type == Event::Closed) {
				wnd.close();
			}
			if (event.type == Event::Resized) {
				Vector2u vec = wnd.getSize();
				string X = to_string(vec.x);
				string Y = to_string(vec.y);
				
				wnd.setTitle("Window: " + X + "px / " + Y + "px");
			}
			/*if (event.type == Event::GainedFocus) {
				Vector2u vec;
				vec.x = 100;
				vec.y = 100;
				wnd.setSize(vec);
			}*/
		}

		if (indicator) {
			sprite2.setPosition(0.f, 0.f);
			sprite3.setPosition(100.f, 500.f);
			sprite4.setPosition(700.f, 300.f);

			indicator = false;
		}

		wnd.clear(Color(255, 183, 246));
		sprite1.setOrigin(112.f, 112.f);
		sprite1.rotate(0.05f);
		
		if (Keyboard::isKeyPressed(Keyboard::D)) {
			sprite2.move(1.f, 0.f);
			sprite3.move(1.f, 0.f);
			sprite4.move(1.f, 0.f);
		}	
			else if (Keyboard::isKeyPressed(Keyboard::A)) {
				sprite2.move(-1.f, 0.f);
				sprite3.move(-1.f, 0.f);
				sprite4.move(-1.f, 0.f);
			}	
			else if (Keyboard::isKeyPressed(Keyboard::W)) {
				sprite2.move(0.f, -1.f);
				sprite3.move(0.f, -1.f);
				sprite4.move(0.f, -1.f);
			}
			else if (Keyboard::isKeyPressed(Keyboard::S)) {
				sprite2.move(0.f, 1.f);
				sprite3.move(0.f, 1.f);
				sprite4.move(0.f, 1.f);
			}
			else if (Mouse::isButtonPressed(Mouse::Button::Left)) {
				auto mousePos = Mouse::getPosition(wnd);
				sprite1.setPosition(Vector2f(mousePos.x, mousePos.y));
			}
			
		wnd.draw(sprite2);		
		wnd.draw(sprite3);	
		wnd.draw(sprite4);
		wnd.draw(sprite1);
		
		wnd.display();

	}


	return 0;
}